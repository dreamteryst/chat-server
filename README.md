# Example Socket.io chat server

## Installation
```
git clone https://gitlab.com/dreamteryst/chat-server.git
cd chat-server
npm install
```

## Starting server

``node index.js``

goto ``http://localhost:3000``